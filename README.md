# colmto-docker

[![Docker Pulls](https://img.shields.io/docker/pulls/socialcars/colmto-docker.svg)](https://hub.docker.com/r/socialcars/colmto-docker/)
[![Docker Automated build](https://img.shields.io/docker/automated/socialcars/colmto-docker.svg)](https://hub.docker.com/r/socialcars/colmto-docker/~/settings/automated-builds/)
[![Docker Build Status](https://img.shields.io/docker/build/socialcars/colmto-docker.svg)](https://hub.docker.com/r/socialcars/colmto-docker/builds/)

Docker container extending [SocialCars/sumo-docker](https://github.com/SocialCars/sumo-docker) with necessary dependencies for running unit-tests on [SocialCars/colmto](https://github.com/SocialCars/colmto).
